import io
import struct


LUA_INT_BYTES = 8
LUA_INT_FORMAT = 'q'
LUA_NUM_BYTES = 8
LUA_NUM_FORMAT = 'd'


def unpack_size(buffer: io.BufferedIOBase):
    ret = 0
    while (b := buffer.read(1)[0]) < 0x80:
        ret = (ret << 7) + b
    return (ret << 7) + b - 0x80


def unpack_int(buffer: io.BufferedIOBase):
    return unpack_size(buffer)


def unpack_integer(buffer: io.BufferedIOBase):
    return struct.unpack(LUA_INT_FORMAT, buffer.read(LUA_INT_BYTES))[0]


def unpack_number(buffer: io.BufferedIOBase):
    return struct.unpack(LUA_NUM_FORMAT, buffer.read(LUA_NUM_BYTES))[0]


def unpack_string(buffer: io.BufferedIOBase):
    size = unpack_size(buffer)
    return buffer.read(size - 1) if size else b''
