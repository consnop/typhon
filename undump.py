import io
from dataclasses import dataclass
from itertools import count
from math import ceil, log10
from typing import Any, Callable, Generic, TypeVar

from lua54_instructions import make_instruction, Lua54Instruction
from values import Value, read_value, String
from unpack import unpack_int, unpack_size, unpack_string


proto_id = count()


T = TypeVar('T')
class UndumpList(list[T]):
    _read: Callable[[io.BufferedIOBase], T]

    def __init__(self, buffer: io.BufferedIOBase):
        read = self.__class__._read
        super().__init__(read(buffer) for _ in range(unpack_size(buffer)))


class Header:
    def __init__(self, buffer: io.BufferedIOBase):
        buffer.read(31)


class Code:
    """List of instructions."""


class Lua54Code(Code, UndumpList[Lua54Instruction]):
    _read = make_instruction

    def __str__(self):
        width = ceil(log10(len(self)))
        return '\n'.join(f'{str(i).rjust(width)} {inst}'
                         for i, inst in enumerate(self, start=1))


class Constants(UndumpList[Value[Any]]):
    """List of constant values"""
    _read = read_value

    def __str__(self):
        return '\n'.join(str(i) for i in self)


class UpValueDesc:
    """Information about captured variables."""
    def __init__(self, buffer: io.BufferedIOBase):
        self.instack = bool(buffer.read(1)[0])
        """Captured from the parent function's stack. Otherwise from the parent funciton's upvalues."""
        self.idx = buffer.read(1)[0]
        """Index in the parent function's stack/upvalue list."""
        self.kind = buffer.read(1)[0]
        """No clue honestly."""

    def __str__(self):
        return f'instack={self.instack}, idx={self.idx}'


class UpValueDescs(UndumpList[UpValueDesc]):
    """List of upvalue descriptions."""
    _read = UpValueDesc

    def __str__(self):
        if not self:
            return ''
        width = ceil(log10(len(self)))
        return '\n'.join(f'{str(i).rjust(width)} {inst}'
                         for i, inst in enumerate(self, start=1))


class Debug:
    def __init__(self, buffer: io.BufferedIOBase):
        self.lineinfos = LineInfos(buffer)
        self.abslineinfos = AbsLineInfos(buffer)
        self.locvars = LocVars(buffer)
        self.upvalues = UpValues(buffer)


class LineInfos(UndumpList[bytes]):
    _read = lambda x: x.read(1)


class AbsLineInfo:
    def __init__(self, buffer: io.BufferedIOBase):
        self.pc = unpack_int(buffer)
        self.line = unpack_int(buffer)


class AbsLineInfos(UndumpList[AbsLineInfo]):
    _read = AbsLineInfo


class LocVar:
    def __init__(self, buffer: io.BufferedIOBase):
        self.varname = unpack_string(buffer)
        self.startpc = unpack_int(buffer)
        self.endpc = unpack_int(buffer)

class LocVars(UndumpList[LocVar]):
    _read = LocVar


class UpValue:
    def __init__(self, buffer: io.BufferedIOBase):
        self.name = unpack_string(buffer)


class UpValues(UndumpList[UpValue]):
    _read = UpValue

C = TypeVar('C', bound=Code)
D = TypeVar('D', bound=Code)
@dataclass
class Prototype(Generic[C]):
    """Numeric identifier."""
    n: int
    """Data representing a lua function/closure."""
    source: String
    """Name of the source file the function was defined in."""
    linedefined: int
    """Line number of the source file where the function was defined"""
    lastlinedefined: int
    """Line number of the source file corresponding to the end of the function definition."""
    numparams: int
    """Number of parameters."""
    is_vararg: bool
    """Is variadic."""
    maxstacksize: int
    """Size of the "stack"/number of used registers."""
    code: C
    """The bytecode of the function."""
    constants: Constants
    """Constant values used within the function."""
    upvalues: UpValueDescs
    """Variables captured by the closure."""
    prototypes: 'Prototypes[C]'
    """Nested function definitions."""
    debug: Debug
    """Debugging information. May be stripped out."""

    def lift(self, lifter: Callable[['Prototype[C]'], 'Prototype[D]'], code: D) -> 'Prototype[D]':
        return Prototype[D](
            self.n,
            self.source,
            self.linedefined,
            self.lastlinedefined,
            self.numparams,
            self.is_vararg,
            self.maxstacksize,
            code,
            self.constants,
            self.upvalues,
            Prototypes[D](lifter(p) for p in self.prototypes),
            self.debug
        )

    def __str__(self):
        return '\n'.join([
            f'source: {self.source}',
            f'code:\n{self.code}',
            f'constants:\n{self.constants}',
            f'upvalues:\n{self.upvalues}'
        ]) + (f'\n\n{self.prototypes}' if self.prototypes else '')


class Prototypes(list[Prototype[C]]):
    """List of function prototypes."""

    def __str__(self):
        return '\n\n'.join(str(i) for i in self)


def read_prototype(buffer: io.BufferedIOBase) -> Prototype[Lua54Code]:
    return Prototype[Lua54Code](
        n=next(proto_id),
        source=String(unpack_string(buffer)),
        linedefined=unpack_int(buffer),
        lastlinedefined=unpack_int(buffer),
        numparams=buffer.read(1)[0],
        is_vararg=bool(buffer.read(1)[0]),
        maxstacksize=buffer.read(1)[0],
        code=Lua54Code(buffer),
        constants=Constants(buffer),
        upvalues=UpValueDescs(buffer),
        prototypes=Prototypes[Lua54Code](read_prototype(buffer) for _ in range(unpack_size(buffer))),
        debug=Debug(buffer)
    )


class Chunk:
    """Data representing a lua source code file."""
    def __init__(self, buffer: io.BufferedIOBase):
        self.header = Header(buffer)
        """Info about the lua version and stuff."""
        self.sizeupvalues = buffer.read(1)[0]
        self.prototype = read_prototype(buffer)
        """The top level function."""
        del buffer

    def __str__(self):
        return str(self.prototype)
