class Instruction:
    args: tuple[str, ...]
    branching: bool

    def bits(self, low: int, high: int) -> int:
        ...
