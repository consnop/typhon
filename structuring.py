from collections import defaultdict
from dataclasses import dataclass, field
from functools import reduce
from itertools import count
from operator import and_, itemgetter
from typing import Any, Callable, TypeVar, cast, NamedTuple, Iterable, Iterator

import cfg
import ir
from render import RenderData
from undump import Code, Prototype



def debug(x: Any) -> Any:
    # print(x)
    # breakpoint()
    return x


def joinlines(*strings: str) -> str:
    return '\n'.join(filter(None, strings))


def indent(lines: str, n: int = 1) -> str:
    return joinlines(*(n * '    ' + line for line in lines.splitlines()))


def gen_label(counter: Iterator[int] = count()) -> str:
    return f'label_{next(counter)}'


T = TypeVar('T', bound='Node')
def add_label(f: Callable[[T, RenderData, dict['Node', str]], str]) -> Callable[[T, RenderData, dict['Node', str]], str]:
    def g(self: T, render_data: RenderData, labels: dict['Node', str]) -> str:
        s = f(self, render_data, labels)
        return f'::{labels[self.head]}::\n{s}' if self.head in labels else s
    return g


class Node:
    head: 'Node'
    subnodes: frozenset['Node']

    def __hash__(self) -> int:
        return hash(self.subnodes)

    def __contains__(self, e: object):
        return e in self.subnodes

    @classmethod
    def detector(cls, node: 'Node', state: 'WorkingState') -> 'Node | None':
        ...

    def render(self, render_data: RenderData, labels: dict['Node', str], /) -> str: # making them position only makes pyright happy for some reason
        ...

    @classmethod
    def make(cls, node: 'Node', graph: 'Graph') -> 'Node':
        ...

    def new_exit(self, graph: 'Graph') -> 'Exit':
        ...


I = isinstance


class Unconditional(NamedTuple):
    """Unconditional control transfer"""
    succ: Node


class Conditional(NamedTuple):
    """Conditional control transfer"""
    true: Node
    false: Node


class Returns(NamedTuple):
    """Return"""


class Virtualized(NamedTuple):
    """Goto"""


Exit = Unconditional | Conditional | Returns | Virtualized
Terminal = Returns | Virtualized
NoCond = Unconditional | Terminal


@dataclass
class Graph(Code):
    """A structured control flow graph."""
    used_locals: set[int]
    nodes: set[Node]
    edges: dict[Node, Exit]
    entry: Node
    labels: dict[Node, str]
    reverse_edges: dict[Node, set[Node]] = field(init=False)
    dom_map: dict[Node, set[Node]] = field(init=False)

    def __post_init__(self):
        self.reverse_edges = reverse_edge_dict(edges_to_dict(self.edges))
        self.dom_map = dominators(self.reverse_edges, self.entry)

    def __str__(self) -> str:
        if len(self.nodes) == 1:
            return '<Structured graph>'
        else:
            return '<Not fully structured graph>'

    def render(self, render_data: RenderData) -> str:
        if self.used_locals:
            local_map = render_data.local_map
            return joinlines(
                f'local {", ".join(local_map[i] for i in self.used_locals)}',
                self.entry.render(render_data, self.labels)
            )
        else: return self.entry.render(render_data, self.labels)

    def dominates(self, x: Node, y: Node):
        return x in self.dom_map.get(y, {y})


@dataclass
class WorkingState:
    graph: Graph
    entry: Node
    body: set[Node]


@dataclass
class StubBlock(Node):
    id: int

    def __init__(self):
        self.head = self
        self.subnodes = frozenset([self])
        self.id = id(self)

    def __hash__(self) -> int:
        return id(self)

    def render(self, render_data: RenderData, labels: dict[Node, str], invert: bool = False) -> str:
        return ''


@dataclass(eq=False)
class BasicBlock(Node):
    head: Node
    code: list[ir.IRInstruction]
    n: int

    def __init__(self, code: cfg.Node):
        self.code = list(code)
        self.n = code.n
        self.head = self
        self.subnodes = frozenset([self])

    def __hash__(self):
        return self.n

    def render_instruction(self, render_data: RenderData, i: ir.IRInstruction) -> str:
        match i:
            case ir.JumpCond(exp): return f'if {exp.render(render_data)} then'
            case ir.Unimplemented(): return ''
            case _: return i.render(render_data)

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str], invert: bool = False) -> str:
        return joinlines(*(s for i in self.code if (s:=self.render_instruction(render_data, i))))

    def render_condition(self, render_data: RenderData, labels: dict[Node, str], invert: bool) -> str:
        jump_cond = self.code[-1]
        assert I(jump_cond, ir.JumpCond)
        jump_cond = ir.JumpCond(ir.Not(jump_cond.exp), jump_cond.target) if invert else jump_cond
        s = joinlines(
            *(s for i in self.code[:-1] if (s:=self.render_instruction(render_data, i))),
            self.render_instruction(render_data, jump_cond)
        )
        return f'::{labels[self.head]}::\n{s}' if self.head in labels else s


def dest_or_terminal(src: Node, dest: Node, graph: Graph) -> bool:
    return (I(t:=graph.edges[src], Unconditional) and t.succ == dest) or (I(graph.edges[src], Terminal))


@dataclass(eq=False)
class Sequence(Node):
    head: Node
    tail: Node

    def __post_init__(self):
        self.subnodes = frozenset([self.head, self.tail])

    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        return joinlines(self.head.render(render_data, labels), self.tail.render(render_data, labels))

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'Sequence | None':
        graph = state.graph
        if (isinstance(t:=graph.edges[node], Unconditional) and (succ := t.succ)):pass
        return cls(node, succ) if (
            I(t:=graph.edges[node], Unconditional) and (succ := t.succ) and
            graph.reverse_edges[succ] == {node} and
            I(graph.edges[succ], NoCond)
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        return graph.edges[self.tail]


@dataclass(eq=False)
class IfThen(Node):
    head: Node
    true: Node
    invert: bool

    def __post_init__(self):
        self.subnodes = frozenset([self.head, self.true])

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        assert I(self.head, BasicBlock)
        return joinlines(
            self.head.render_condition(render_data, labels, self.invert),
            indent(self.true.render(render_data, labels)),
            'end'
        )

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'IfThen | None':
        graph = state.graph
        return cls(node, then, invert) if ( # pyright: ignore[reportGeneralTypeIssues] # pyright is being insane again ans saying then might equal Literal[True]
            I(t:=graph.edges[node], Conditional) and
            (then := next((
                then
                for then, succ in ((t.true, t.false), (t.false, t.true)) if
                I(graph.edges[succ], NoCond) and
                graph.reverse_edges[then] == {node} and
                dest_or_terminal(then, succ, graph)
            ), None)) and ((invert := then != t.true) or True) # pyright: ignore[reportGeneralTypeIssues]
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        if I(t:=graph.edges[self.true], Unconditional):
            return t
        else:
            assert I(t:=graph.edges[self.head], Conditional)
            return Unconditional(t.false)


@dataclass(eq=False)
class IfThenElse(Node):
    head: Node
    true: Node
    false: Node

    def __post_init__(self):
        self.subnodes = frozenset([self.head, self.true, self.false])

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        return joinlines(
            self.head.render(render_data, labels),
            indent(self.true.render(render_data, labels)),
            'else',
            indent(self.false.render(render_data, labels)),
            'end'
        )

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'IfThenElse | None':
        graph = state.graph
        return cls(node, true, false) if (
            I(t:=graph.edges[node], Conditional) and (true := t.true, false := t.false) and
            graph.reverse_edges[true] == {node} and
            graph.reverse_edges[false] == {node} and
            I(t:=graph.edges[true], Unconditional) and (true_succ := t.succ) and
            I(t:=graph.edges[false], Unconditional) and (false_succ := t.succ) and
            true_succ == false_succ
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        return graph.edges[self.true]


@dataclass(eq=False)
class Goto(Node):
    head: Node
    label: str

    def __post_init__(self):
        self.subnodes = frozenset([self.head])

    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        return joinlines(self.head.render(render_data, labels), f'goto {self.label}')

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'Goto | None':
        if (
            I(node, BasicBlock | StubBlock) and
            I(t:=state.graph.edges[node], Unconditional) and
            t.succ not in state.body - {state.entry}
        ):
            label = gen_label()
            state.graph.labels[t.succ] = label
            return cls(node, label)
        return None

    def new_exit(self, graph: 'Graph') -> 'Exit':
        return Virtualized()


@dataclass(eq=False)
class NumericFor(Node):
    prep: Node
    head: Node
    body: Node

    def __post_init__(self):
        self.subnodes = frozenset([self.prep, self.head, self.body])

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        assert I(self.prep, BasicBlock)
        for_prep = self.prep.code[0]
        assert I(for_prep, ir.ForPrep)
        assert I(self.head, BasicBlock)
        for_loop = self.head.code[0]
        assert I(for_loop, ir.ForLoop)
        return joinlines(
            f'for {for_loop.external} = {for_prep.internal_val}, {for_prep.limit_val}, {for_prep.step_val} do',
            indent(self.body.render(render_data, labels)),
            'end'
        )

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'NumericFor | None':
        graph = state.graph
        return cls(prep, node, body) if (
            I(t:=graph.edges[node], Conditional) and (body := t.true) and
            (prep := next(
                prep for prep in graph.reverse_edges[node]
                if prep != body
            ))
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        header_exit = graph.edges[self.head]
        assert I(header_exit, Conditional)
        return Unconditional(header_exit.false)


@dataclass(eq=False)
class While(Node):
    head: Node
    body: Node
    inverted_cond: bool

    def __post_init__(self):
        self.subnodes = frozenset([self.head, self.body])

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        assert I(self.head, BasicBlock)
        assert len(self.head.code) == 1
        i = self.head.code[0]
        assert I(i, ir.JumpCond)
        cond = ir.Not(i.exp) if self.inverted_cond else i.exp
        return joinlines(f'while {cond.render(render_data)} do', indent(self.body.render(render_data, labels)), 'end')

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'While | None':
        graph = state.graph
        return cls(node, latch, latch == false) if (
            I(node, BasicBlock) and
            len(node.code) == 1 and
            I(node.code[0], ir.JumpCond) and
            I(t:=graph.edges[node], Conditional) and (true := t.true, false := t.false) and
            (latch:=next(filter(lambda x: I(t:=graph.edges[x], Unconditional) and node == t.succ, (true, false)), None)) and
            graph.dominates(node, latch)
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        old_exit = graph.edges[self.head]
        assert isinstance(old_exit, Conditional)
        return Unconditional(old_exit.true) if self.inverted_cond else Unconditional(old_exit.false)


@dataclass(eq=False)
class DoWhile(Node):
    head: Node
    body: Node
    inverted_cond: bool

    def __post_init__(self):
        self.subnodes = frozenset([self.head, self.body])

    @add_label
    def render(self, render_data: RenderData, labels: dict[Node, str]) -> str:
        assert isinstance(self.head, BasicBlock)
        assert len(self.head.code) == 1
        i = self.head.code[0]
        assert isinstance(i, ir.JumpCond)
        cond = i.exp if self.inverted_cond else ir.Not(i.exp)
        return joinlines('repeat', indent(self.body.render(render_data, labels)), f'until {cond.render(render_data)}')

    @classmethod
    def detector(cls, node: Node, state: WorkingState) -> 'DoWhile | None':
        graph = state.graph
        return cls(head, body, body == false) if (
            (body := node) and
            I(t:=graph.edges[body], Unconditional) and (head := t.succ) and
            I(head, BasicBlock) and
            len(head.code) == 1 and
            I(head.code[0], ir.JumpCond) and
            I(t:=graph.edges[head], Conditional) and (true := t.true, false := t.false) and
            tuple(... for i in (true, false) if (i == body)) and
            graph.dominates(body, head)
        ) else None

    def new_exit(self, graph: Graph) -> Exit:
        old_exit = graph.edges[self.head]
        assert isinstance(old_exit, Conditional)
        return Unconditional(old_exit.true) if self.inverted_cond else Unconditional(old_exit.false)


@dataclass(frozen=True)
class SelfLoop(Node):
    head: Node


def edges_to_dict(edges: dict[Node, Exit]) -> dict[Node, set[Node]]:
    return {k: set(v) for k, v in edges.items()}


def reverse_edge_dict(edges: dict[Node, set[Node]]) -> dict[Node, set[Node]]:
    d = defaultdict[Node, set[Node]](set)
    for k, v in edges.items():
        for e in v:
            d[e].add(k)
    return d


def dominators(reverse_edges: dict[Node, set[Node]], entry: Node) -> dict[Node, set[Node]]:
    dominators = dict[Node, set[Node]]()
    dominators[entry] = set([entry])
    for node in reverse_edges.keys():
        if node != entry:
            dominators[node] = set(reverse_edges.keys()) | {entry, node}
    changed = True
    while changed:
        changed = False
        for n, preds in reverse_edges.items():
            if n == entry: continue
            new = reduce(and_, (dominators.get(pred, set()) for pred in preds)) | {n}
            changed |= new != dominators[n]
            dominators[n] = new
    return dominators


def translate(cfg_graph: cfg.Graph) -> Graph:
    edges = dict[Node, Exit]()
    translated = dict[cfg.Node, Node]()

    def get_translation(n: cfg.Node):
        return translated.setdefault(n, BasicBlock(n))

    for node in cfg_graph.nodes:
        match cfg_graph.edges[node]:
            case cfg.Unconditional(succ):
                edges[get_translation(node)] = Unconditional(get_translation(succ))
            case cfg.Conditional(true, false):
                edges[get_translation(node)] = Conditional(get_translation(true), get_translation(false))
            case cfg.Returns():
                edges[get_translation(node)] = Returns()
    return Graph(
        used_locals,
        set(map(get_translation, cfg_graph.nodes)),
        edges,
        get_translation(cfg_graph.entry),
        {}
    )


def collapse(graph: Graph, subgraph: Node) -> Graph:
    def collapsed_node(n: Node) -> Node:
        return subgraph if n in subgraph else n

    nodes = (graph.nodes - subgraph.subnodes) | {subgraph}
    edges = {subgraph: subgraph.new_exit(graph)} | {k: v._make(map(collapsed_node, v)) for k, v in graph.edges.items() if k not in subgraph}
    return Graph(used_locals, nodes, edges, collapsed_node(graph.entry), graph.labels)


def collapse_opt(structure_cls: type[Node], state: WorkingState, target: Node) -> Graph | None:
    subgraph = structure_cls.detector(target, state)
    return (
        collapse(state.graph, subgraph) if
            subgraph and
            not (
                structure_cls in (IfThen, IfThenElse) and
                any(
                    node in state.graph.labels
                    for node in subgraph.subnodes - {subgraph.head}
                )
            )
        else None
    )


def expand_nodes(nodes: frozenset[Node] | set[Node]) -> frozenset[BasicBlock]:
    def expand_one(nodes: frozenset[Node] | set[Node]) -> frozenset[Node]:
        return reduce(lambda x, y: x | y.subnodes, nodes, frozenset[Node]())

    return cast(frozenset[BasicBlock], fixpoint(expand_one, nodes))


def new_entry(old_entry: Node, old_nodes: set[Node], new_nodes: set[Node]) -> Node:
    return old_entry if old_entry in new_nodes else next(iter(new_nodes - old_nodes))


def new_state(old: WorkingState, new_graph: Graph, in_nodes: set[Node]) -> WorkingState:
    new_lexical_body = (old.body - in_nodes) | (new_graph.nodes - old.graph.nodes)
    return WorkingState(new_graph, new_entry(old.entry, old.body, new_lexical_body), new_lexical_body)


def post_order(edges: dict[Node, Exit], working_set: set[Node], entry: Node):
    visited = set[Node]()
    def dfs(current: Node) -> Iterable[Node]:
        visited.add(current)
        for node in edges[current]:
            if node in working_set and node not in visited:
                yield from dfs(node)
        yield current

    return dfs(entry)


def loop_semantic_body(graph: Graph, header: Node, latch: Node) -> set[Node]:
    visited = set[Node]([header])
    def dfs(current: Node):
        visited.add(current)
        for node in graph.reverse_edges[current]:
            if node not in visited: dfs(node)
    dfs(latch)
    return visited


def reachable(graph: Graph, working_set: set[Node], successor: Node) -> set[Node]:
    return set(post_order(graph.edges, working_set, successor))


def loop_body(graph: Graph, working_set: set[Node], header: Node, latch: Node, loop_cls: type[NumericFor | While | DoWhile | SelfLoop]) -> set[Node]:
    # NOTE: true/false could be swapped, but apparently the compiler doesn't do that
    if loop_cls in (While, NumericFor): successor = cast(Conditional, graph.edges[header]).false
    elif loop_cls == DoWhile: successor = cast(Conditional, graph.edges[latch]).true
    else: assert False
    if loop_cls == NumericFor:
        for_prep = {next(prep for prep in graph.reverse_edges[header] if prep != latch)}
    else:
        for_prep = set[Node]()
    semantic_body = loop_semantic_body(graph, header, latch)
    syntactic_complement = {node for node in graph.nodes if graph.dominates(header, node)} - reachable(graph, working_set - {header}, successor)
    return semantic_body | syntactic_complement | for_prep


def top_wrapper(graph: Graph) -> Graph:
    return debug(structure(WorkingState(graph, graph.entry, graph.nodes), set(), None).graph)


def loop_refinement(state: WorkingState, loop_cls: type[NumericFor | While | DoWhile | SelfLoop], header: Node, latch: Node) -> Graph:
    if loop_cls == While:
        exclude = {header}
    elif loop_cls == NumericFor:
        prep = next(prep for prep in state.graph.reverse_edges[header] if prep != latch)
        exclude = {prep, header}
    else:
        exclude = {latch}
    state = structure(state, exclude, latch)
    loop = loop_cls.detector(state.entry, state)
    assert loop, "couldn't match loop"
    return collapse(state.graph, loop)


def loop_candidate_cls(graph: Graph, header: Node, latch: Node) -> type[NumericFor | While | DoWhile | SelfLoop]:
    if (
        I(header, BasicBlock) and
        len(header.code) == 1 and
        I(header.code[0], ir.ForLoop)
    ):
        return NumericFor
    elif I(graph.edges[header], Conditional):
        return While
    elif I(graph.edges[latch], Conditional):
        return DoWhile
    else:
        assert False
        return SelfLoop


def stub_conditional_gotos(state: WorkingState, exclude: set[Node], latch: Node | None) -> WorkingState:
    def stub_exit(state: WorkingState, node: Node):
        outs = list[Node]()
        exits = dict[Node, Exit]()
        for dest in state.graph.edges[node]:
            if dest not in state.body or (node != latch and dest == state.entry):
                stub = StubBlock()
                outs.append(stub)
                exits |= {stub: Unconditional(dest)}
            else: outs.append(dest)
        return WorkingState(
            Graph(
                state.graph.used_locals,
                state.graph.nodes | exits.keys(),
                state.graph.edges
                    | exits
                    | {node: Conditional(*outs)},
                state.graph.entry,
                state.graph.labels
            ),
            state.entry,
            state.body | exits.keys()
        ) if exits else state

    return reduce(
        stub_exit,
        (
            node for node in state.body
            if node not in exclude and I(state.graph.edges[node], Conditional)
        ),
        state
    )


def linear_refine(state: WorkingState, exclude: set[Node], latch: Node | None) -> WorkingState:
    for node in post_order(state.graph.edges, state.body, state.entry):
        if node in exclude: continue
        for reduction in (IfThen, IfThenElse, Sequence, Goto):
            if reduction == Goto and node == latch: continue
            if (new_graph := collapse_opt(reduction, state, node)):
                return new_state(state, new_graph, state.body - new_graph.nodes)
    return state


def get_candidate_edges(state: WorkingState): # TODO: reimplement ghidra's heuristic https://github.com/NationalSecurityAgency/ghidra/blob/fa8eff4d33f6323bcd8e26615d5a362046bb2756/Ghidra/Features/Decompiler/src/decompile/cpp/blockaction.hh#L78
    return state

    def push_branchpoint(out: Conditional): 
        push_trace(out.true)
        push_trace(out.false)
        push_trace(out.true)

    def push_trace(dest: Node): ...


def sort_edges(): ...


def virtualize(): ...


def last_resort_refine(state: WorkingState) -> WorkingState:
    get_candidate_edges(state)
    sort_edges()
    virtualize()
    return state


U = TypeVar('U')
def fixpoint(f: Callable[..., U], init: U, *extra: Any) -> U:
    state = init
    while state != (new := f(state, *extra)) and (state := new): pass
    return state


def structure(state: WorkingState, exclude: set[Node], latch: Node | None) -> WorkingState:
    def all_possible_loop_bodies(header: Node, graph: Graph)-> list[tuple[Node, set[Node]]]:
        return [
            (latch, loop_body(graph, state.body, header, latch, loop_candidate_cls(graph, header, latch)))
            for latch in graph.reverse_edges[header] if graph.dominates(header, latch)
        ]

    def proper_subsets(latches_and_bodies: list[tuple[Node, set[Node]]], nodes: set[Node]) -> list[tuple[Node, set[Node]]]:
        return [(latch, lexical) for latch, lexical in latches_and_bodies if lexical < nodes]

    def loop_refine(state: WorkingState) -> WorkingState:
        return next(
            (
                new_state(
                    state,
                    loop_refinement(WorkingState(state.graph, header, body), loop_candidate_cls(state.graph, header, latch), header, latch),
                    body
                )
                for header in post_order(state.graph.edges, state.body, state.entry) if (
                    (latches_and_bodies := proper_subsets(all_possible_loop_bodies(header, state.graph), state.body)) and
                    (t := cast(tuple[Node, set[Node]], max(latches_and_bodies, key=itemgetter(1)))) and (latch := t[0], body := t[1]) # pyright: ignore[reportUnnecessaryCast] # pyright bug
                )
            ),
            state
        )

    def refine(state: WorkingState) -> WorkingState:
        state = fixpoint(loop_refine, state)
        state = stub_conditional_gotos(state, exclude, latch)
        state = fixpoint(linear_refine, state, exclude, latch)
        state = last_resort_refine(state)
        return state

    state = fixpoint(refine, state)
    return state


def get_used_locals(proto: Prototype[cfg.Graph]) -> set[int]:
    return {
        dest.register
        for node in proto.code.nodes
        for inst in node if I(inst, ir.Assign)
        for dest in inst.dests if I(dest, ir.Local) and dest.register >= proto.numparams
    }


def lifter(proto: Prototype[cfg.Graph]) -> Prototype[Graph]:
    global used_locals # ugly hack, used only for rendering
    used_locals = get_used_locals(proto)
    return proto.lift(lifter, debug(top_wrapper(translate(proto.code))))
