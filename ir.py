"""
A higher level intermediate representation for lua programs more
suitable for subsequent analysis.
"""

from dataclasses import dataclass
from itertools import count
from typing import Any

from instruction import Instruction
from render import RenderData
from values import Integer, Value
from undump import Code


def list_str(exps: list[Any]) -> str:
    return ', '.join(str(i) for i in exps)


def list_render(exps: list[Any], render_data: RenderData) -> str:
    return ', '.join(i.render(render_data) for i in exps)


@dataclass(frozen=True)
class Label:
    n: int

    def __str__(self):
        return f'label{self.n:0>3}'


class Statement:
    def __str__(self):
        return self.__class__.__name__


class IRInstruction(Statement, Instruction):
    def render(self, render_data: RenderData) -> str:
        ...


class CodeExpression(Statement):
    def render(self, render_data: RenderData) -> str:
        ...


Expression = Value[Any] | CodeExpression


class Location(CodeExpression):
    pass


@dataclass(frozen=True)
class Local(Location):
    register: int

    def __str__(self):
        return f'local{self.register}'

    def render(self, render_data: RenderData) -> str:
        return render_data.local_map[self.register]


@dataclass(frozen=True)
class Global(Location):
    name: str

    def __str__(self):
        return self.name

    def render(self, render_data: RenderData) -> str:
        return str(self)


@dataclass(frozen=True)
class UpValue(Location):
    index: int

    def __str__(self):
        return f'upval{self.index}'

    def render(self, render_data: RenderData) -> str:
        return render_data.upval_map[self.index]


@dataclass(frozen=True)
class TableLookup(Location):
    table: Location
    key: Expression

    def __str__(self):
        return f'{self.table}[{self.key}]'

    def render(self, render_data: RenderData) -> str:
        return f'{self.table.render(render_data)}[{self.key.render(render_data)}]'


@dataclass(frozen=True)
class BinaryExpression(CodeExpression):
    left: Expression
    right: Expression


class BinaryOperation(BinaryExpression):
    operator = ''

    def __str__(self):
        return f'({self.left} {self.operator} {self.right})'

    def render(self, render_data: RenderData) -> str:
        return f'({self.left.render(render_data)} {self.operator} {self.right.render(render_data)})'


class Addition(BinaryOperation):
    operator = '+'


class Subtraction(BinaryOperation):
    operator = '-'


class Multiplication(BinaryOperation):
    operator = '*'


class Modulo(BinaryOperation):
    operator = '%'


class Power(BinaryOperation):
    operator = '^'


class Division(BinaryOperation):
    operator = '/'


class IntDivision(BinaryOperation):
    operator = '//'


class BitAnd(BinaryOperation):
    operator = '&'


class BitOr(BinaryOperation):
    operator = '|'


class BitXor(BinaryOperation):
    operator = '~'


class ShiftRight(BinaryOperation):
    operator = '>>'


class ShiftLeft(BinaryOperation):
    operator = '<<'


@dataclass(frozen=True)
class UnaryOperation(CodeExpression):
    operator = ''
    exp: Expression

    def __str__(self):
        return f'{self.operator}{self.exp}'

    def render(self, render_data: RenderData) -> str:
        return f'{self.operator}{self.exp.render(render_data)}'


class UnaryMinus(UnaryOperation):
    operator = '-'


class BitNot(UnaryOperation):
    operator = '~'


class Not(UnaryOperation):
    operator = 'not '


class Length(UnaryOperation):
    operator = '#'


@dataclass(frozen=True)
class Concat(CodeExpression):
    operands: list[Expression]

    def __str__(self):
        return ' .. '.join(str(i) for i in self.operands)

    def render(self, render_data: RenderData) -> str:
        return ' .. '.join(i.render(render_data) for i in self.operands)


class Comparison(BinaryExpression):
    operator = ''

    def __str__(self):
        return f'{self.left} {self.operator} {self.right}'

    def render(self, render_data: RenderData) -> str:
        return f'{self.left.render(render_data)} {self.operator} {self.right.render(render_data)}'


class Equal(Comparison):
    operator = '=='


class LessThan(Comparison):
    operator = '<'


class LessEqual(Comparison):
    operator = '<='


@dataclass(frozen=True)
class Closure(CodeExpression):
    index: int
    captures: set[int]

    def __str__(self):
        return f'closure{self.index}'

    def render(self, render_data: RenderData) -> str:
        return render_data.nested_definitions[self.index]


class VarArg(CodeExpression):
    def __str__(self):
        return 'vararg'


@dataclass(frozen=True)
class Top(Location):
    bottom: int
    maxstacksize: int

    def __str__(self):
        return f'loc{self.bottom:0>3} ... top'


@dataclass
class Call(CodeExpression):
    closure: Expression
    arguments: list[Expression]

    def __str__(self):
        return f'{self.closure}({list_str(self.arguments)})'

    def render(self, render_data: RenderData) -> str:
        return f'{self.closure.render(render_data)}({list_render(self.arguments, render_data)})'


@dataclass(frozen=True)
class Assign(IRInstruction):
    dests: list[Location]
    source: Expression

    def __str__(self):
        return f'{list_str(self.dests)} = {self.source}' if self.dests else str(self.source)

    def render(self, render_data: RenderData) -> str:
        return f'{list_render(self.dests, render_data)} = {self.source.render(render_data)}' if self.dests else self.source.render(render_data)


@dataclass(frozen=True)
class Close(IRInstruction):
    index: int


@dataclass(frozen=True)
class Jump(IRInstruction):
    target: Label

    def __str__(self):
        return f'Jump {self.target}'


@dataclass(frozen=True)
class JumpCond(IRInstruction):
    exp: Expression
    target: Label

    def __str__(self):
        return f'JumpCond ({self.exp}) {self.target}'


@dataclass(frozen=True)
class ForLoop(IRInstruction):
    internal: Local
    limit: Local
    step: Local
    external: Local
    target: Label

    def __str__(self):
        return f'ForLoop {self.external}={self.internal}, {self.limit}, {self.step}: {self.target}'


@dataclass(frozen=True)
class ForPrep(IRInstruction):
    internal: Local
    limit: Local
    step: Local
    internal_val: Integer
    limit_val: Integer
    step_val: Integer
    target: Label

    def __str__(self):
        return f'ForPrep {self.internal}={self.internal_val}, {self.limit}={self.limit_val}, {self.step}={self.step_val}: {self.target}'


@dataclass(frozen=True)
class Return(IRInstruction):
    return_vals: list[Expression]

    def __str__(self):
        return f'Return {list_str(self.return_vals)}'

    def render(self, render_data: RenderData) -> str:
        match self.return_vals:
            case []: return ''
            case xs: return f'return {", ".join(x.render(render_data) for x in xs)}'


class TForLoop(IRInstruction):
    pass


class ToBeClosed(IRInstruction):
    pass


@dataclass(frozen=True)
class Unimplemented(IRInstruction):
    comment: str

    def __str__(self) -> str:
        return f'Unimplemented({self.comment})'


class IRCode(Code, list[IRInstruction]):
    label_to_pos: dict[Label, int]
    pos_to_label: dict[int, Label]

    def __init__(self, instructions: list[IRInstruction] = [], label_pos: list[tuple[Label, int]] = []):
        super().__init__(instructions)
        self.label_to_pos = {k: v for k, v in label_pos}
        self.pos_to_label = {k: v for v, k in label_pos}

    def __str__(self):
        lines = list[str]()
        for i, inst in enumerate(self):
            if i in self.pos_to_label:
                lines.append(f'{self.pos_to_label[i]}:')
            lines.append(f'    {inst}')
        return '\n'.join(lines)


class CodeGen:
    def __init__(self):
        self.from_inst = 0
        self.code: list[IRInstruction] = []
        self._instruction_count = count(1)
        self._instruction_map: dict[int, int] = {}
        self._label_map: dict[int, Label] = {}
        self._label_count = count(1)
        self.labels: list[Label] = []
        self._instruction_map[0] = 0
        self._label_map[0] = Label(0)

    def next(self):
        next(self._instruction_count)

    def append(self, inst: IRInstruction):
        self._instruction_map.setdefault(self.from_inst, len(self.code))
        self.code.append(inst)

    def emit(self):
        self.from_inst = next(self._instruction_count)

    def get_label(self, target: int, absolute: bool = False) -> Label:
        if not absolute:
            target = self.from_inst + target + 1
        label = Label(next(self._label_count))
        return self._label_map.setdefault(target, label)

    def generate(self) -> IRCode:
        label_pos = [(v, self._instruction_map[k]) for k, v in self._label_map.items()]
        return IRCode(self.code, label_pos)
