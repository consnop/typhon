import io
import struct

from instruction import Instruction


def make_instruction(buffer: io.BufferedIOBase):
    instruction_bytes = buffer.read(4)
    return op_table[Lua54Instruction(instruction_bytes).Op](instruction_bytes)


class Lua54Instruction(Instruction):
    args = ()
    branching = False

    def __init__(self, buffer: bytes):
        self._uint = struct.unpack('I', buffer)[0]
        self.Op = self.bits(0, 6)
        self.A = self.bits(7, 14)
        self.k = self.bits(15, 15)
        self.B = self.bits(16, 23)
        self.C = self.bits(24, 31)
        self.sB = self.B - (1 << 7) + 1
        self.sC = self.C - (1 << 7) + 1
        self.Bx = self.bits(15, 31)
        self.sBx = self.Bx - (1 << 16) + 1
        self.Ax = self.bits(7, 31)
        self.sJ = self.bits(7, 31) - (1 << 24) + 1

    def __str__(self):
        ret = self.__class__.__name__.upper().ljust(11)
        for arg in self.args:
            value = getattr(self, arg)
            if arg == 'k':
                if value:
                    ret = ret.ljust(26) + 'k'
                continue
            negative = value < 0
            if 'Bx' in arg:
                width = 10
            elif arg in ('Ax', 'sJ'):
                width = 15
            else:
                width = 5
            if not negative:
                ret += ' '
            ret += str(value).ljust(width - (not negative))
        return ret

    def bits(self, low: int, high: int) -> int:
        mask = sum(1 << i for i in range(low, high + 1))
        return (self._uint & mask) >> low


class Move(Lua54Instruction):
    args = ('A', 'B')


class LoadI(Lua54Instruction):
    args = ('A', 'sBx')


class LoadF(Lua54Instruction):
    args = ('A', 'sBx')


class LoadK(Lua54Instruction):
    args = ('A', 'Bx')


class LoadKX(Lua54Instruction):
    args = ('A',)


class LoadFalse(Lua54Instruction):
    args = ('A',)


class LFalseSkip(Lua54Instruction):
    args = ('A',)


class LoadTrue(Lua54Instruction):
    args = ('A',)


class LoadNil(Lua54Instruction):
    args = ('A', 'B')


class GetUpVal(Lua54Instruction):
    args = ('A', 'B')


class SetUpVal(Lua54Instruction):
    args = ('A', 'B')


class GetTabUp(Lua54Instruction):
    args = ('A', 'B', 'C')


class GetTable(Lua54Instruction):
    args = ('A', 'B', 'C')


class GetI(Lua54Instruction):
    args = ('A', 'B', 'C')


class GetField(Lua54Instruction):
    args = ('A', 'B', 'C')


class SetTabUp(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class SetTable(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class SetI(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class SetField(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class NewTable(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class Self(Lua54Instruction):
    args = ('A', 'B', 'C')


class AddI(Lua54Instruction):
    args = ('A', 'B', 'sC')


class AddK(Lua54Instruction):
    args = ('A', 'B', 'C')


class SubK(Lua54Instruction):
    args = ('A', 'B', 'C')


class MulK(Lua54Instruction):
    args = ('A', 'B', 'C')


class ModK(Lua54Instruction):
    args = ('A', 'B', 'C')


class PowK(Lua54Instruction):
    args = ('A', 'B', 'C')


class DivK(Lua54Instruction):
    args = ('A', 'B', 'C')


class IDivK(Lua54Instruction):
    args = ('A', 'B', 'C')


class BAndK(Lua54Instruction):
    args = ('A', 'B', 'C')


class BOrK(Lua54Instruction):
    args = ('A', 'B', 'C')


class BXorK(Lua54Instruction):
    args = ('A', 'B', 'C')


class ShRI(Lua54Instruction):
    args = ('A', 'B', 'sC')


class ShLI(Lua54Instruction):
    args = ('A', 'B', 'sC')


class Add(Lua54Instruction):
    args = ('A', 'B', 'C')


class Sub(Lua54Instruction):
    args = ('A', 'B', 'C')


class Mul(Lua54Instruction):
    args = ('A', 'B', 'C')


class Mod(Lua54Instruction):
    args = ('A', 'B', 'C')


class Pow(Lua54Instruction):
    args = ('A', 'B', 'C')


class Div(Lua54Instruction):
    args = ('A', 'B', 'C')


class IDiv(Lua54Instruction):
    args = ('A', 'B', 'C')


class BAnd(Lua54Instruction):
    args = ('A', 'B', 'C')


class BOr(Lua54Instruction):
    args = ('A', 'B', 'C')


class BXor(Lua54Instruction):
    args = ('A', 'B', 'C')


class ShL(Lua54Instruction):
    args = ('A', 'B', 'C')


class ShR(Lua54Instruction):
    args = ('A', 'B', 'C')


class MMBin(Lua54Instruction):
    args = ('A', 'B', 'C')


class MMBinI(Lua54Instruction):
    args = ('A', 'sB', 'C', 'k')


class MMBinK(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class UnM(Lua54Instruction):
    args = ('A', 'B')


class BNot(Lua54Instruction):
    args = ('A', 'B')


class Not(Lua54Instruction):
    args = ('A', 'B')


class Len(Lua54Instruction):
    args = ('A', 'B')


class Concat(Lua54Instruction):
    args = ('A', 'B')


class Close(Lua54Instruction):
    args = ('A',)


class TBC(Lua54Instruction):
    args = ('A',)


class Jmp(Lua54Instruction):
    args = ('sJ',)


class Eq(Lua54Instruction):
    args = ('A', 'B', 'k')
    branching = True


class LT(Lua54Instruction):
    args = ('A', 'B', 'k')
    branching = True


class LE(Lua54Instruction):
    args = ('A', 'B', 'k')
    branching = True


class EqK(Lua54Instruction):
    args = ('A', 'B', 'k')
    branching = True


class EqI(Lua54Instruction):
    args = ('A', 'sB', 'k')
    branching = True


class LTI(Lua54Instruction):
    args = ('A', 'sB', 'k')
    branching = True


class LEI(Lua54Instruction):
    args = ('A', 'sB', 'k')
    branching = True


class GTI(Lua54Instruction):
    args = ('A', 'sB', 'k')
    branching = True


class GEI(Lua54Instruction):
    args = ('A', 'sB', 'k')
    branching = True


class Test(Lua54Instruction):
    args = ('A', 'k')
    branching = True


class TestSet(Lua54Instruction):
    args = ('A', 'B', 'k')
    branching = True


class Call(Lua54Instruction):
    args = ('A', 'B', 'C')


class TailCall(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class Return(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class Return0(Lua54Instruction):
    args = ()


class Return1(Lua54Instruction):
    args = ('A',)


class ForLoop(Lua54Instruction):
    args = ('A', 'Bx')
    branching = True


class ForPrep(Lua54Instruction):
    args = ('A', 'Bx')
    branching = True


class TForPrep(Lua54Instruction):
    branching = True
    args = ('A', 'Bx')


class TForCall(Lua54Instruction):
    args = ('A', 'C')


class TForLoop(Lua54Instruction):
    args = ('A', 'Bx')
    branching = True


class SetList(Lua54Instruction):
    args = ('A', 'B', 'C', 'k')


class Closure(Lua54Instruction):
    args = ('A', 'Bx')


class VarArg(Lua54Instruction):
    args = ('A', 'C')


class VarArgPrep(Lua54Instruction):
    args = ('A',)


class ExtraArg(Lua54Instruction):
    args = ('Ax',)


op_table = [
    Move,
    LoadI,
    LoadF,
    LoadK,
    LoadKX,
    LoadFalse,
    LFalseSkip,
    LoadTrue,
    LoadNil,
    GetUpVal,
    SetUpVal,
    GetTabUp,
    GetTable,
    GetI,
    GetField,
    SetTabUp,
    SetTable,
    SetI,
    SetField,
    NewTable,
    Self,
    AddI,
    AddK,
    SubK,
    MulK,
    ModK,
    PowK,
    DivK,
    IDivK,
    BAndK,
    BOrK,
    BXorK,
    ShRI,
    ShLI,
    Add,
    Sub,
    Mul,
    Mod,
    Pow,
    Div,
    IDiv,
    BAnd,
    BOr,
    BXor,
    ShL,
    ShR,
    MMBin,
    MMBinI,
    MMBinK,
    UnM,
    BNot,
    Not,
    Len,
    Concat,
    Close,
    TBC,
    Jmp,
    Eq,
    LT,
    LE,
    EqK,
    EqI,
    LTI,
    LEI,
    GTI,
    GEI,
    Test,
    TestSet,
    Call,
    TailCall,
    Return,
    Return0,
    Return1,
    ForLoop,
    ForPrep,
    TForPrep,
    TForCall,
    TForLoop,
    SetList,
    Closure,
    VarArg,
    VarArgPrep,
    ExtraArg
]
