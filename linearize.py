"""Linearize the CFG to make it readable."""

from dataclasses import replace
from queue import Queue

import cfg
import ir
from undump import Prototype


def lifter(proto: Prototype[cfg.Graph]) -> Prototype[ir.IRCode]:
    graph = proto.code
    linear_code = list[ir.IRInstruction]()
    node_to_label = {node: ir.Label(node.n) for node in graph.nodes}
    node_to_pos = dict[cfg.Node, int]()
    v = set[cfg.Node]()
    q = Queue[cfg.Node]()
    q.put(graph.entry)
    while not q.empty():
        node = q.get()
        if node in v:
            continue
        v.add(node)
        node_to_pos[node] = len(linear_code)
        linear_code.extend(node)
        match graph.edges[node]:
            case cfg.Conditional(true=true, false=false):
                q.put(true)
                q.put(false)
                jump_cond: ir.IRInstruction = linear_code[-1]
                if isinstance(jump_cond, ir.JumpCond) or isinstance(jump_cond, ir.ForLoop):
                    linear_code[-1] = replace(jump_cond, target=node_to_label[true])
                    linear_code.append(ir.Jump(node_to_label[false]))
            case cfg.Unconditional(node=node):
                q.put(node)
                linear_code.append(ir.Jump(node_to_label[node]))
            case cfg.Returns(): pass
    label_pos = [(node_to_label[node], node_to_pos[node]) for node in node_to_pos]

    return proto.lift(lifter, ir.IRCode(linear_code, label_pos))
