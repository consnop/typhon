# Typhon
A WIP Lua decompiler.

### Limitations
currently unsupported features:
- variadic functions
- `for` loops
- `break` statements
- `goto` statements
- short circuiting operators (`and`, `or`)
- to-be-closed variables

Local variable scoping is also unimplemented which may cause issues when functions capture scoped variables.

There is also lots of room for prettyfication of statements like function declarations, table initialization and `elseif` statements.