import io
import typing
from dataclasses import dataclass
from typing import TypeVar

from unpack import unpack_integer, unpack_number, unpack_string
from render import RenderData


T_co = TypeVar('T_co', covariant=True)
class Value(typing.Generic[T_co]):
    value: T_co

    def __str__(self):
        return str(self.value)

    def render(self, render_data: RenderData) -> str:
        return str(self)


def read_value(buffer: io.BufferedIOBase):
    type_tag = buffer.read(1)[0]
    match type_tag:
        case 0x13: return Number(unpack_number(buffer))
        case 0x03: return Integer(unpack_integer(buffer))
        case (0x14 | 0x04): return String(unpack_string(buffer))
        case (0x01 | 0x11): return Bool(type_tag == 0x11)
        case 0x00: return Nil()
        case _: assert False, 'Unknown constant type'


class Table(Value[str]):
    value: str = '{}'


@dataclass
class Number(Value[float]):
    value: float


@dataclass
class Integer(Value[int]):
    value: int


@dataclass
class String(Value[bytes]):
    value: bytes
    def __str__(self):
        return repr(self.value.decode('unicode_escape'))


@dataclass
class Bool(Value[bool]):
    value: bool


@dataclass
class Nil(Value[None]):
    def __str__(self):
        return 'nil'
