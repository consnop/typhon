from typing import Type

import ir
from lua54_instructions import *
from undump import Prototype, Lua54Code
from values import *


global_upval_table = dict[int, int | None]()


def lifter(proto: Prototype[Lua54Code]) -> Prototype[ir.IRCode]:
    globals_upvalue = global_upval_table[id(proto)]
    i: Lua54Instruction

    def make_get_table(key: ir.Expression) -> list[ir.IRInstruction]:
        table = ir.Local(i.B)
        return [ir.Assign([ir.Local(i.A)], ir.TableLookup(table, key))]

    def make_set_table(key: ir.Expression) -> list[ir.IRInstruction]:
        table = ir.Local(i.A)
        source = rk(i.C)
        return [ir.Assign([ir.TableLookup(table, key)], source)]

    def make_bin_op_k(op: Type[ir.BinaryOperation]) -> list[ir.IRInstruction]:
        right = proto.constants[i.C]
        assert isinstance(right, Number)
        source = op(ir.Local(i.B), right)
        return [ir.Assign([ir.Local(i.A)], source)]

    def make_bin_op(op: Type[ir.BinaryOperation]) -> list[ir.IRInstruction]:
        source = op(ir.Local(i.B), ir.Local(i.C))
        return [ir.Assign([ir.Local(i.A)], source)]

    def make_un_op(op: Type[ir.UnaryOperation]) -> list[ir.IRInstruction]:
        source = op(ir.Local(i.B))
        return [ir.Assign([ir.Local(i.A)], source)]

    def rk(x: int):
        return proto.constants[x] if i.k else ir.Local(x)

    def make_comp(exp: ir.Comparison) -> list[ir.IRInstruction]:
        cond = ir.Not(exp) if i.k else exp
        return [ir.JumpCond(cond, cg.get_label(1))]

    cg = ir.CodeGen()
    it = iter(proto.code)
    for i in it:
        ic: list[ir.IRInstruction]
        match i:
            case Move():
                ic = [ir.Assign([ir.Local(i.A)], ir.Local(i.B))]
            case LoadI():
                ic = [ir.Assign([ir.Local(i.A)], Integer(i.sBx))]
            case LoadF():
                ic = [ir.Assign([ir.Local(i.A)], Number(i.sBx))]
            case LoadK():
                ic = [ir.Assign([ir.Local(i.A)], proto.constants[i.Bx])]
            case LoadKX():
                dest = ir.Local(i.A)
                cg.next()
                i = next(it)
                source = proto.constants[i.Ax]
                ic = [ir.Assign([dest], source)]
            case LoadFalse():
                ic = [ir.Assign([ir.Local(i.A)], Bool(False))]
            case LFalseSkip():
                ic = [
                    ir.Assign([ir.Local(i.A)], Bool(False)),
                    ir.Jump(cg.get_label(1))
                ]
            case LoadTrue():
                ic = [ir.Assign([ir.Local(i.A)], Bool(True))]
            case LoadNil():
                ic = [ir.Assign([ir.Local(i.A)], Nil())]
            case GetUpVal():
                ic = [ir.Assign([ir.Local(i.A)], ir.UpValue(i.B))]
            case SetUpVal():
                ic = [ir.Assign([ir.UpValue(i.B)], ir.Local(i.A))]
            case GetTabUp():
                key = proto.constants[i.C]
                assert isinstance(key, String)
                if i.B == globals_upvalue:
                    ic = [ir.Assign([ir.Local(i.A)], ir.Global(key.value.decode()))]
                else:
                    source = ir.TableLookup(ir.UpValue(i.B), key)
                    ic = [ir.Assign([ir.Local(i.A)], source)]
            case GetTable():
                key = ir.Local(i.C)
                ic = make_get_table(key)
            case GetI():
                key = Integer(i.C)
                ic = make_get_table(key)
            case GetField():
                key = proto.constants[i.C]
                assert isinstance(key, String)
                ic = make_get_table(key)
            case SetTabUp():
                key = proto.constants[i.B]
                assert isinstance(key, String)
                if i.A == globals_upvalue:
                    ic = [ir.Assign([ir.Global(key.value.decode())], rk(i.C))]
                else:
                    dest = ir.TableLookup(ir.UpValue(i.A), key)
                    ic = [ir.Assign([dest], rk(i.C))]
            case SetTable():
                key = ir.Local(i.B)
                ic = make_set_table(key)
            case SetI():
                key = Integer(i.B)
                ic = make_set_table(key)
            case SetField():
                key = proto.constants[i.B]
                assert isinstance(key, String)
                ic = make_set_table(key)
            case NewTable():
                ic = [ir.Assign([ir.Local(i.A)], Table())]
                cg.next()
                next(it)
            case Self():
                table = ir.Local(i.B)
                key = rk(i.C)
                if isinstance(key, Value):
                    assert isinstance(key, String)
                ic = [
                    ir.Assign([ir.Local(i.A + 1)], ir.Local(i.B)),
                    ir.Assign([ir.Local(i.A)], ir.TableLookup(table, key))
                ]
            case AddI():
                source = ir.Addition(ir.Local(i.B), Integer(i.sC))
                ic = [ir.Assign([ir.Local(i.A)], source)]
            case AddK():
                ic = make_bin_op_k(ir.Addition)
            case SubK():
                ic = make_bin_op_k(ir.Subtraction)
            case MulK():
                ic = make_bin_op_k(ir.Multiplication)
            case ModK():
                ic = make_bin_op_k(ir.Modulo)
            case PowK():
                ic = make_bin_op_k(ir.Power)
            case DivK():
                ic = make_bin_op_k(ir.Division)
            case IDivK():
                ic = make_bin_op_k(ir.IntDivision)
            case BAndK():
                ic = make_bin_op_k(ir.BitAnd)
            case BOrK():
                ic = make_bin_op_k(ir.BitOr)
            case BXorK():
                ic = make_bin_op_k(ir.BitXor)
            case ShRI():
                source = ir.ShiftRight(ir.Local(i.B), Integer(i.C))
                ic = [ir.Assign([ir.Local(i.A)], source)]
            case ShLI():
                source = ir.ShiftLeft(Integer(i.C), ir.Local(i.B))
                ic = [ir.Assign([ir.Local(i.A)], source)]
            case Add():
                ic = make_bin_op(ir.Addition)
            case Sub():
                ic = make_bin_op(ir.Subtraction)
            case Mul():
                ic = make_bin_op(ir.Multiplication)
            case Mod():
                ic = make_bin_op(ir.Modulo)
            case Pow():
                ic = make_bin_op(ir.Power)
            case Div():
                ic = make_bin_op(ir.Division)
            case IDiv():
                ic = make_bin_op(ir.IntDivision)
            case BAnd():
                ic = make_bin_op(ir.BitAnd)
            case BOr():
                ic = make_bin_op(ir.BitOr)
            case BXor():
                ic = make_bin_op(ir.BitXor)
            case ShL():
                ic = make_bin_op(ir.ShiftLeft)
            case ShR():
                ic = make_bin_op(ir.ShiftRight)
            case MMBin() | MMBinI() | MMBinK():
                ic = []
            case UnM():
                ic = make_un_op(ir.UnaryMinus)
            case BNot():
                ic = make_un_op(ir.BitNot)
            case Not():
                ic = make_un_op(ir.Not)
            case Len():
                ic = make_un_op(ir.Length)
            case Concat():
                ic = [ir.Assign([ir.Local(i.A)], ir.Concat([ir.Local(r) for r in range(i.A, i.A + i.B)]))]
            case Close():
                ic = [ir.Close(i.A)]
            case TBC():
                ic = [ir.ToBeClosed()]
            case Jmp():
                ic = [ir.Jump(cg.get_label(i.sJ))]
            case Eq():
                ic = make_comp(ir.Equal(ir.Local(i.A), ir.Local(i.B)))
            case LT():
                ic = make_comp(ir.LessThan(ir.Local(i.A), ir.Local(i.B)))
            case LE():
                ic = make_comp(ir.LessEqual(ir.Local(i.A), ir.Local(i.B)))
            case EqK():
                ic = make_comp(ir.Equal(ir.Local(i.A), proto.constants[i.B]))
            case EqI():
                ic = make_comp(ir.Equal(ir.Local(i.A), Integer(i.sB)))
            case LTI():
                ic = make_comp(ir.LessThan(ir.Local(i.A), Integer(i.sB)))
            case LEI():
                ic = make_comp(ir.LessEqual(ir.Local(i.A), Integer(i.sB)))
            case GTI():
                ic = make_comp(ir.LessThan(Integer(i.sB), ir.Local(i.A)))
            case GEI():
                ic = make_comp(ir.LessEqual(Integer(i.sB), ir.Local(i.A)))
            case Test():
                exp = ir.Local(i.A)
                cond = ir.Not(exp) if i.k else exp
                ic = [ir.JumpCond(cond, cg.get_label(1))]
            case TestSet():
                exp = ir.Local(i.B)
                cond = ir.Not(exp) if i.k else exp
                ic = [
                    ir.JumpCond(cond, cg.get_label(1)),
                    ir.Assign([ir.Local(i.A)], ir.Local(i.B))
                ]
            case Call():
                ic = [ir.Assign([ir.Local(r) for r in range(i.A, i.A + i.C - 1)], ir.Call(ir.Local(i.A), [ir.Local(r) for r in range(i.A + 1, i.A + i.B)]))] # pyright: ignore [reportGeneralTypeIssues] # I think this is a pyright bug?
            case TailCall():
                ic = [ir.Assign([ir.Local(r) for r in range(i.A, i.A + i.C - 1)], ir.Call(ir.Local(i.A), [ir.Local(r) for r in range(i.A + 1, i.A + i.B)]))] # pyright: ignore [reportGeneralTypeIssues] # I think this is a pyright bug?
            case Return(): # need to finish vararg stuff
                ic = [ir.Return([ir.Local(r) for r in range(i.A, i.B - 1)])]
            case Return0():
                ic = [ir.Return([])]
            case Return1():
                ic = [ir.Return([ir.Local(i.A)])]
            case ForLoop():
                ic = [ir.ForLoop(ir.Local(i.A), ir.Local(i.A + 1), ir.Local(i.A + 2), ir.Local(i.A + 3), cg.get_label(-i.Bx))]
            case ForPrep():
                cg.from_inst -= 3
                load_step = cg.code.pop()
                assert isinstance(load_step, ir.Assign)
                step_val = load_step.source
                assert isinstance(step_val, Integer)
                load_limit = cg.code.pop()
                assert isinstance(load_limit, ir.Assign)
                limit_val = load_limit.source
                assert isinstance(limit_val, Integer)
                load_init = cg.code.pop()
                assert isinstance(load_init, ir.Assign)
                internal_val = load_init.source
                assert isinstance(internal_val, Integer)
                source = ir.Subtraction(ir.Local(i.A), ir.Local(i.A + 1))
                ic = [ir.ForPrep(
                    ir.Local(i.A),
                    ir.Local(i.A + 1),
                    ir.Local(i.A + 2),
                    internal_val,
                    limit_val,
                    step_val,
                    cg.get_label(i.Bx + 3)
                )]
            case TForPrep() | TForCall() | TForLoop():
                ic = [ir.Unimplemented(i.__class__.__name__)] # unimplemented
            case SetList():
                dests = [ir.TableLookup(ir.Local(i.A), Integer(i.C + j)) for j in range(1, i.B + 1)]
                sources = [ir.Local(j) for j in range(i.A + 1, i.A + i.B + 1)]
                ic = [ir.Assign([dest], source) for dest, source in zip(dests, sources)]
            case Closure():
                ic = [ir.Assign([ir.Local(i.A)], ir.Closure(i.Bx, set()))]
            case VarArg() | VarArgPrep():
                ic = [ir.Unimplemented(i.__class__.__name__)] # unimplemented
            case ExtraArg():
                ic = []
            case _:
                assert False
        for j in ic:
            cg.append(j)
        cg.emit()
    return proto.lift(lifter, cg.generate())
