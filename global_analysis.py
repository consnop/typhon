from typing import Iterable, Optional, TypeVar

from undump import Prototype, Code


C = TypeVar('C', bound=Code)
def iterate_prototypes(parent_id: int, root: Prototype[C]) -> Iterable[tuple[int, Prototype[C]]]:
    yield parent_id, root
    for proto in root.prototypes:
        yield from iterate_prototypes(id(root), proto)


def trace_global_upval(proto: Prototype[C]) -> dict[int, Optional[int]]:
    root = proto
    res: dict[int, Optional[int]] = {}
    res[id(root)] = 0
    for child in root.prototypes:
        for parent_id, proto in iterate_prototypes(id(root), child):
            parent_global_index = res[parent_id]
            for i, upval_desc in enumerate(proto.upvalues):
                if not upval_desc.instack and upval_desc.idx == parent_global_index:
                    res[id(proto)] = i
                    break
            else:
                res[id(proto)] = None
    return res
