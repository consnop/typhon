from dataclasses import dataclass
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import structuring as struct
    from undump import Prototype


@dataclass
class RenderData:
    nested_definitions: list[str]
    local_map: dict[int, str]
    upval_map: dict[int, str]


def render(proto: 'Prototype[struct.Graph]') -> str:
    def wrap(string: str, x: 'Prototype[struct.Graph]', local_map: dict[int, str]) -> str:
        return (
            f'function({", ".join(local_map[i] for i in range(x.numparams))})\n    ' +
            '\n    '.join(string.splitlines()) +
            '\n' +
            'end\n'
        )

    def rec(proto: 'Prototype[struct.Graph]', parent_local_map: dict[int, str], parent_upval_map: dict[int, str], func: bool = True) -> str:
        local_map = {i: f'local{proto.n}_{i}' for i in range(proto.maxstacksize)}
        upval_map = {i: parent_local_map[e.idx] if e.instack else parent_upval_map[e.idx] for i, e in enumerate(proto.upvalues)}
        nested = [rec(i, local_map, upval_map) for i in proto.prototypes]
        return (
            wrap(proto.code.render(RenderData(nested, local_map, upval_map)), proto, local_map)
            if func
            else proto.code.render(RenderData(nested, local_map, upval_map))
        )

    return rec(proto, {0: '_ENV'}, {}, func=False)
