"""
Control flow graph

Functional python code is a little cursed, consider yourself warned.
"""

from collections import Counter, defaultdict, deque
from dataclasses import dataclass, replace
from functools import reduce
from itertools import count, chain
from operator import attrgetter, itemgetter
from typing import Any, Callable, TypeVar, cast, NamedTuple, Iterable

import ir
from undump import Prototype, Code


T = TypeVar('T')
def pipeline(x: T, *fs: Callable[[T], T]) -> T:
    return reduce(lambda x, f: f(x), fs, x)


def nested_replace(d: Any, o: Any, path: tuple[int | str, ...]) -> Any:
    """
    Poor man's lenses.

    The syntax tree of IR expressions and instructions consists of dataclasses and lists.
    Paths down the tree can be specified through a combination of strings for field names and ints for list indices.
    """
    match path:
        case ():
            return o
        case _:
            match path[0]:
                case str() as field:
                    return replace(d, **{field: nested_replace(getattr(d, field), o, path[1:])})
                case int() as i:
                    assert isinstance(d, list)
                    return cast(Any, d[:i] + [nested_replace(cast(list[Any], d[i]), o, path[1:])] + d[i + 1:])


def make_replacer(*path: int | str) -> 'FreeSubstitution':
    return cast(FreeSubstitution, lambda o: lambda d: nested_replace(d, o, path)) # pyright: ignore [reportUnknownLambdaType]


class Node(list[ir.IRInstruction]):
    """
    A node in the control flow graph.

    Contains informations on variables defined and referenced on each line
    """
    _count = count(1)

    def __init__(self):
        self.n = next(self._count)

    def __repr__(self):
        return f'Node {self.n}'

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Node) and self.n == other.n

    def __hash__(self): # pyright: ignore [reportIncompatibleVariableOverride]
        return self.n


class Unconditional(NamedTuple):
    """Unconditional control transfer (jump)"""
    node: Node


class Conditional(NamedTuple):
    """Conditional control transfer (jump cond)"""
    true: Node
    false: Node


class Returns(NamedTuple):
    """Return"""


Exit = Unconditional | Conditional | Returns
Register = int


class Position(NamedTuple):
    """Position in the CFG"""
    node: Node
    line: int

    def stmt_str(self):
        return str(self.node[self.line])


@dataclass(frozen=True)
class CodeDefinition:
    """Regular variable definition obtained from the original code."""
    register: Register
    position: Position
    simple: bool
    """getting the value of a global or upvalue"""

    def __str__(self):
        return f'CodeDefinition({self.position.stmt_str()})'


@dataclass(frozen=True)
class CaptureDefinition:
    """Potential definition of a captured variable by a funciton call."""
    register: Register
    position: Position

    def __str__(self):
        return f'CaptureDefinition({self.register}, {self.position.stmt_str()})'


@dataclass(frozen=True)
class DangerousDefinition:
    """Definition which can not be safely propagated."""
    register: Register


@dataclass(unsafe_hash=True)
class PhiDefinition:
    """Phi definition injected to ensure only one definition of each variable is live."""
    argument_map = dict['PhiDefinition', set['Definition']]()
    register: Register
    node: Node

    def __init__(self, register: Register, node: Node, arguments: set['Definition']):
        self.register = register
        self.node = node
        self.argument_map[self] = arguments

    def __eq__(self, other: object):
        return isinstance(other, self.__class__) and self.register == other.register and self.node == other.node

    def __getattribute__(self, name: str):
        if name == 'arguments':
            return self.argument_map[self]
        return super().__getattribute__(name)

    def __str__(self) -> str:
        return f'Phi({self.register}, {self.node})'


@dataclass(frozen=True)
class ZeroDefinition:
    """The initial value at the beginning of the function."""
    register: Register


Definition = CodeDefinition | DangerousDefinition | PhiDefinition | ZeroDefinition


@dataclass(frozen=True)
class CodeReference:
    """Regular variable reference obtained from the original code."""
    register: Register
    position: Position
    replacer: 'FreeSubstitution'

    def __str__(self) -> str:
        return f'CodeReference({self.register}, {self.position.stmt_str()})'


@dataclass(frozen=True)
class CaptureReference:
    """Reference caused by the application of a closure."""
    register: Register
    position: Position

    def __str__(self) -> str:
        return f'CaptureReference({self.register}, {self.position.stmt_str()})'


@dataclass(frozen=True)
class PhiReference:
    register: Register


Reference = CodeReference | CaptureReference | PhiReference


def get_node_definitions(node: Node, captures: tuple[ir.Local, ...]) -> dict[int, list[Definition]]:
    return {
        i: get_line_definitions(inst, Position(node, i), captures)
        for i, inst in enumerate(node)
    }


def get_line_definitions(inst: ir.IRInstruction, pos: Position, captures: tuple[ir.Local, ...]) -> list[Definition]:
    pre, ret = [], []
    match inst:
        case ir.Assign():
            pre = [CaptureDefinition(i.register, pos) for i in captures] if isinstance(inst.source, ir.Call) else []
            match inst.dests:
                case [ir.Local() as l]:
                    if isinstance(inst.source, ir.Closure):
                        ret = [DangerousDefinition(i.register) for i in inst.dests if isinstance(i, ir.Local)]
                    else:
                        ret = [CodeDefinition(l.register, pos, type(inst.source) in (ir.UpValue, ir.Global))]
                case [ir.TableLookup(table=t)]:
                    assert isinstance(t, ir.Local)
                    ret = [DangerousDefinition(t.register)]
                case _:
                    ret = [DangerousDefinition(i.register) for i in inst.dests if isinstance(i, ir.Local)]
        case ir.ForPrep():
            ret = [
                CodeDefinition(inst.internal.register, pos, False),
                CodeDefinition(inst.limit.register, pos, False),
                CodeDefinition(inst.step.register, pos, False)
            ]
        case ir.ForLoop():
            ret = [CodeDefinition(inst.internal.register, pos, False), CodeDefinition(inst.external.register, pos, False)]
        case _: pass
    return cast(list[Definition], [i for i in pre if i.register not in map(attrgetter('register'), ret)] + ret)


def get_node_references(node: Node, captures: tuple[ir.Local, ...]) -> dict[int, list[Reference]]:
    return {i: get_line_references(inst, Position(node, i), captures) for i, inst in enumerate(node)}


def get_line_references(inst: ir.IRInstruction, pos: Position, captures: tuple[ir.Local, ...]) -> list[Reference]:
    ret = list[Reference]()
    match inst:
        case ir.Assign() as assn:
            pre = [CaptureReference(i.register, pos) for i in captures] if isinstance(assn.source, ir.Call) else []
            ret += [
                CodeReference(reg, pos, make_replacer('dests', i, 'key', *path))
                for i, e in enumerate(assn.dests)
                if isinstance(e, ir.TableLookup) and isinstance(e.key, ir.CodeExpression)
                for reg, path in reference_iterator(e.key)
            ]
            if isinstance(s := assn.source, ir.CodeExpression):
                ret += [CodeReference(reg, pos, make_replacer('source', *path)) for reg, path in reference_iterator(s)]
            ret = [i for i in pre if i.register not in map(attrgetter('register'), ret)] + ret
        case ir.JumpCond(exp=e):
            if isinstance(e, ir.CodeExpression):
                ret += [CodeReference(reg, pos, make_replacer('exp', *path)) for reg, path in reference_iterator(e)]
        case ir.ForLoop(internal, limit, step):
            ret += [
                CodeReference(internal.register, pos, make_replacer('internal')),
                CodeReference(limit.register, pos, make_replacer('limit')),
                CodeReference(step.register, pos, make_replacer('step'))
            ]
        case ir.Return(return_vals=return_vals):
            ret += [
                CodeReference(reg, pos, make_replacer('return_vals', i, *path))
                for i, e in enumerate(return_vals) if isinstance(e, ir.CodeExpression)
                for reg, path in reference_iterator(e)
            ]
        case _: pass
    return ret


def reference_iterator(exp: ir.CodeExpression) -> Iterable[tuple[int, tuple[str | int, ...]]]:
    if isinstance(exp, ir.Local):
        yield exp.register, ()
    else:
        for attr_name in dir(exp):
            if attr_name.startswith('__'): continue
            match attr := getattr(exp, attr_name):
                case list() :
                    for i, e in enumerate(j for j in cast(list[Any], attr) if isinstance(j, ir.CodeExpression)):
                        for reg, op in reference_iterator(e):
                            yield reg, (attr_name, i, *op)
                case ir.CodeExpression():
                    for reg, op in reference_iterator(attr):
                        yield reg, (attr_name, *op)
                case _: pass


@dataclass(frozen=True)
class Graph(Code):
    """A control flow graph."""
    entry: Node
    edges: dict[Node, Exit]
    nodes: set[Node]


def inject_phi_functions(graph: Graph, numparam: int, captures: tuple[ir.Local, ...]) -> dict[Node, dict[Register, Definition]]:
    """Initialize the block definitions for the graph and inject phi functions."""

    def reduce_defs(register: Register, node: Node, defs: set[Definition]) -> Definition:
        return PhiDefinition(register, node, defs) if len(defs) > 1 else next(iter(defs))

    in_defs = defaultdict[Node, dict[Register, Definition]](dict)
    params = cast(dict[Register, Definition], {r: ZeroDefinition(r) for r in range(numparam)} | {-1: ZeroDefinition(-1)})
    in_defs[graph.entry] = params.copy()
    definitions_masks = {
        node: {
            l.register: l
            for v in get_node_definitions(node, captures).values()
            for l in v
        }
        for node in graph.nodes
    }
    reverse_edges: dict[Node, set[Node]] = {graph.entry: set()}
    for u, out_node in graph.edges.items():
         for v in out_node: reverse_edges.setdefault(v, set()).add(u)
    old_in_defs = {}
    while in_defs != old_in_defs: # fixpoint
        old_in_defs = in_defs
        out_defs = {node: in_defs[node] | definitions_masks[node] for node in graph.nodes}
        in_defined: dict[Node, set[Register]] = {
            node: set(chain.from_iterable(out_defs[src_node].keys() for src_node in reverse_edges[node]))
            for node in graph.nodes
        }
        in_defs = {
            node: {
                register: reduce_defs(
                    register,
                    node,
                    {out_defs[src_node][register] for src_node in reverse_edges[node] if register in out_defs[src_node]}
                )
                for register in in_defined[node]
            }
            for node in graph.nodes
        }
        in_defs[graph.entry] = params.copy()
    return in_defs


@dataclass
class RefDefNode:
    refs: list[tuple[Reference, 'RefDefNode']]
    defs: list[Definition]
    live_defs: dict[Register, Definition]

    def __hash__(self):
        return id(self)

    def __str__(self) -> str:
        return '\n'.join(
            chain(
                (f'References: {(str(r[0]), str(r[1]))}' for r in self.refs),
                (f'Defines: {d}' for d in self.defs)
            )
        )


def resolve_defs(graph: Graph, in_defs: dict[Node, dict[Register, Definition]], captures: tuple[ir.Local, ...]) -> list[RefDefNode]:
    """Determine which definition belongs to which reference."""
    def_to_rdn = dict[Definition, RefDefNode]()
    # the graph contains loops, so we can't just build the nodes as we go
    raw_rdns = list[tuple[list[tuple[Reference, Definition]], RefDefNode]]()
    for node in graph.nodes:
        current_defs = in_defs[node].copy()
        def_to_rdn |= {d: RefDefNode([], [d], {}) for d in current_defs.values() if isinstance(d, ZeroDefinition)}
        def_to_rdn |= (phi_rdns := {d: RefDefNode([], [d], {}) for d in current_defs.values() if isinstance(d, PhiDefinition) if d.node == node})
        raw_rdns += [
            ([(PhiReference(d.register), j) for j in d.arguments], rdn)
            for d, rdn in phi_rdns.items()
        ]
        for i, inst in enumerate(node):
            pos = Position(node, i)
            references = get_line_references(inst, pos, captures)
            definitions = get_line_definitions(inst, pos, captures)
            if definitions or references:
                rdn = RefDefNode([], definitions, current_defs.copy())
                raw_rdns.append(([(r, current_defs[r.register]) for r in references], rdn))
                def_to_rdn |= {definition: rdn for definition in definitions}
            if isinstance(inst, ir.Assign):
                current_defs |= {
                    definition.register: definition
                    for definition in definitions
                }
    for d, rdn in raw_rdns: rdn.refs = [(i, def_to_rdn[j]) for i, j in d]
    return list(map(itemgetter(1), raw_rdns))


def eliminate_dead_defs(xs: list[RefDefNode]) -> tuple[list[RefDefNode], Counter[RefDefNode]]:
    def phi_rdn(rdn: RefDefNode) -> bool:
        return len(rdn.defs) == 1 and isinstance(rdn.defs[0], PhiDefinition)

    reference_counts = Counter[RefDefNode](chain(*map(lambda x: map(itemgetter(1), (i for i in x.refs if i[0].register != -1)), xs)))
    phi_to_phi = defaultdict[RefDefNode, set[RefDefNode]](set)
    for e in xs:
        if not phi_rdn(e): continue
        for _, d in e.refs:
            if not phi_rdn(d): continue
            phi_to_phi[d].add(e)
    for i in phi_to_phi.values():
        assert all(phi_rdn(j) for j in i)
    skip = set[RefDefNode]()
    keep = set[RefDefNode]()
    for x in xs:
        if not phi_rdn(x) or x in skip or x in keep: continue
        q = deque[RefDefNode]([x])
        v = set[RefDefNode]()
        good = False
        while q:
            assert all(phi_rdn(i) for i in q), 'q is corrupted'
            e = q.popleft()
            if e in v: continue
            v.add(e)
            if len(phi_to_phi[e]) == reference_counts[e]:
                assert all(phi_rdn(i) for i in phi_to_phi[e]), 'phi_to_phi is corrupted'
                q.extend(phi_to_phi[e])
            else:
                good = True
        if good: keep |= v
        else: skip |= v
    for i in skip:
        for _, j in i.refs:
            reference_counts[j] -= 1
    return [i for i in xs if i not in skip], reference_counts


def safe_substitutions(def_ref_nodes: list[RefDefNode]) -> 'FreeSubstitutions':

    def recursive_lookup(d: Definition, m: dict[Definition, Definition]) -> Definition:
        return d if d not in m or m[d] == d else recursive_lookup(m[d], m)

    def check_lifetime_overlap() -> dict[Definition, Definition] | None:
        for _, k in node.refs:
            for d in k.defs:
                masked = map(lambda x: recursive_lookup(x, mask), e.live_defs.values())
                if not d in map(lambda x: node.live_defs.get(x.register, x), masked):
                    return None
        return {d: recursive_lookup(node.live_defs[d.register], mask) for d in node.defs if d.register in node.live_defs}

    def_ref_nodes, reference_counts = eliminate_dead_defs(def_ref_nodes)
    approved = FreeSubstitutions()
    mask = dict[Definition, Definition]()
    first: None | RefDefNode = None # makes sure we don't loop forever

    q = deque[RefDefNode](filter( # remove phi definitions
        lambda x: not (len(x.defs) == 1 and isinstance(x.defs[0], PhiDefinition)),
        def_ref_nodes
    ))
    # NOTE: I'm not quite sure yet, but we might be able to improve time complexity by going in post order
    while q and (e := q.popleft()) != first: # pyright: ignore # pyright bug
        keep = False
        for ref, node in e.refs: # for each edge
            if not isinstance(ref, CodeReference): continue
            match [d for d in node.defs if not isinstance(d, CaptureDefinition)]:
                case [CodeDefinition() as cd] if reference_counts[node] == 1: # only definitions we're allowed to propagate
                    # check it's dependencies
                    if (mask_update := check_lifetime_overlap()) is None:
                        first = first or e
                    else:
                        mask |= mask_update # they need to be added to the mask
                        approved[cd.position] = (ref.replacer, ref.position) # substitution needs to be approved
                        e.refs.remove((ref, node))
                        e.refs.extend((node.refs))
                        first = None
                    keep = True
                case [CodeDefinition(simple=True) as cd]:
                    approved[cd.position] = (ref.replacer, ref.position)
                    e.refs.remove((ref, node))
                    first = None
                    keep = True
                case _: pass
        if keep: q.append(e)
    return approved


def construct(proto: Prototype[ir.IRCode]):
    """Construct a graph from a Prototype[ir.Code] object."""
    code = proto.code
    entry = Node()
    edges: dict[Node, Exit] = {}
    pos_to_node: dict[int, Node] = {0: entry}
    q = deque[tuple[int, Node]]([(0, entry)])
    visited = set[Node]()
    start = 0
    node = entry

    while q:
        start, node = q.popleft()
        if node in visited:
            continue
        visited.add(node)
        for i in range(start, len(code)):
            match code[i]:
                case _ if start != i and i in code.pos_to_label:
                    successor = pos_to_node.setdefault(i, Node())
                    edges[node] = Unconditional(successor)
                    q.append((i, successor))
                case ir.Jump(target=label):
                    successor = pos_to_node.setdefault(code.label_to_pos[label], Node())
                    edges[node] = Unconditional(successor)
                    q.append((code.label_to_pos[label], successor))
                case ir.JumpCond(target=label) | ir.ForLoop(target=label):
                    new_node = pos_to_node.setdefault(i, Node())
                    edges[node] = Unconditional(new_node)
                    new_node.append(code[i])
                    jump_node = pos_to_node.setdefault(code.label_to_pos[label], Node())
                    fallthrough_node = pos_to_node.setdefault(i + 1, Node())
                    edges[new_node] = Conditional(jump_node, fallthrough_node)
                    q.append((code.label_to_pos[label], jump_node))
                    q.append((i + 1, fallthrough_node))
                case ir.ForPrep(target=label):
                    new_node = pos_to_node.setdefault(i, Node())
                    edges[node] = Unconditional(new_node)
                    new_node.append(code[i])
                    successor = pos_to_node.setdefault(code.label_to_pos[label], Node())
                    edges[new_node] = Unconditional(successor)
                    q.append((code.label_to_pos[label], successor))
                case ir.Return():
                    node.append(code[i])
                    edges[node] = Returns()
                case _:
                    node.append(code[i])
                    continue
            break
    return Graph(entry, edges, set(pos_to_node.values()))


def eliminate_jump_chains(graph: Graph) -> Graph:
    """Rewrite chains of jumps to a single jump where possible."""
    def is_jump_link(node: Node) -> bool:
        match node:
            case []:
                return True
            case _:
                return False

    def follow_chain(node: Node) -> Node:
        if not is_jump_link(node):
            return node
        match graph.edges[node]:
            case Unconditional(successor):
                return follow_chain(successor)
            case _: assert False

    def simplify_exit(exit: Exit):
        match exit:
            case Unconditional(node):
                return Unconditional(follow_chain(node))
            case Conditional(true, false):
                return Conditional(follow_chain(true), follow_chain(false))
            case Returns():
                return Returns()

    nodes = {node for node in graph.nodes if not is_jump_link(node)}
    edges = {node: simplify_exit(graph.edges[node]) for node in nodes}
    return Graph(graph.entry, edges, nodes)


def cut_jumpconds(graph: Graph) -> Graph:
    mask = dict[Node, tuple[Node, Node]]()
    for node in graph.nodes:
        match node[-1]:
            case ir.JumpCond() if len(node) > 1:
                head = Node()
                head.extend(node[:-1])
                tail = Node()
                tail.append(node[-1])
                mask[node] = (head, tail)
            case _: pass
    new_nodes = set[Node]()
    for node in graph.nodes:
        if node in mask:
            new_nodes.add(mask[node][0])
            new_nodes.add(mask[node][1])
        else:
            new_nodes.add(node)
    new_edges = dict[Node, Exit]()
    for node, exit in graph.edges.items():
        if node in mask:
            node = mask[node][1]
        exit = exit._make(mask[i][0] if i in mask else i for i in exit)
        new_edges[node] = exit
    for head, tail in mask.values():
        new_edges[head] = Unconditional(tail)
    return Graph(graph.entry if graph.entry not in mask else mask[graph.entry][0], new_edges, new_nodes)


ClosedSubstitution = Callable[[ir.IRInstruction], ir.IRInstruction]
FreeSubstitution = Callable[[ir.Expression], ClosedSubstitution]
class ClosedSubstitutions(dict[Position, list[ClosedSubstitution]]): ...
class FreeSubstitutions(dict[Position, tuple[FreeSubstitution, Position]]): ...
class Substitutions(tuple[ClosedSubstitutions, FreeSubstitutions]): ...
class PropagationState(tuple[Substitutions, dict[Node, Node]]): ...


def traversal(graph: Graph) -> Iterable[Node]:
    visited = set[Node]()
    q = deque[Node]([graph.entry])
    while q:
        u = q.popleft()
        if u in visited:
            continue
        visited.add(u)
        yield u
        for v in graph.edges[u]:
            q.append(v)


def translate_exit(exit: Exit, node_map: dict[Node, Node]) -> Exit:
    return type(exit)(*(node_map[node] for node in exit))

def process_instruction(state: PropagationState, pos: Position) -> PropagationState:
    (cs_map, fs_map), node_map = state
    inst = pipeline(pos.node[pos.line], *cs_map.pop(pos, []))
    if pos in fs_map: # this instruction is a definition we want to propagate and eliminate
        assert isinstance(inst, ir.Assign)
        free_substitution, new_pos = fs_map.pop(pos)
        cs_map.setdefault(new_pos, []).append(free_substitution(inst.source))
    else:
        node_map[pos.node].append(inst) # should not be eliminated, add to new graph
    return PropagationState((Substitutions((cs_map, fs_map)), node_map))


def process_node(state: PropagationState, node: Node) -> PropagationState:
    state[1].update({node: Node()}) # there are no other live references to this, so it's safe to mutate
    return reduce(process_instruction, (Position(node, i) for i in range(len(node))), state)


def translate_graph(graph: Graph, node_map: dict[Node, Node]) -> Graph:
    return Graph(node_map[graph.entry], {node_map[node]: translate_exit(graph.edges[node], node_map) for node in graph.nodes}, set(node_map.values()))


def propagate(graph: Graph, free_substitutions: FreeSubstitutions) -> Graph:
    return translate_graph(
        graph,
        reduce(
            process_node,
            traversal(graph),
            PropagationState((Substitutions((ClosedSubstitutions(), free_substitutions)), {}))
        )[1]
    )


def get_captures(proto: Prototype[ir.IRCode]) -> Iterable[ir.Local]:
    return (ir.Local(u.idx) for p in proto.prototypes for u in p.upvalues if u.instack)


def lifter(proto: Prototype[ir.IRCode]) -> Prototype[Graph]:
    captures = (*get_captures(proto), ir.Local(-1)) # represents external state
    graph = eliminate_jump_chains(construct(proto))
    graph = propagate(
        graph,
        safe_substitutions(
            resolve_defs(
                graph,
                inject_phi_functions(graph, proto.numparams, captures),
                captures
            )
        )
    )
    graph = eliminate_jump_chains(graph)
    graph = cut_jumpconds(graph)
    return proto.lift(lifter, graph)
